#!/bin/bash
#set this to name of your certificate, used as part of path to your Let's Encrypt certificate 
certificate_name="charlieblog.eu"

#flexibee settings for automatic certificate upload to server
flexibee_name="jmeno"
flexibee_pass="heslo"

# convert key to PKCS#1 format
openssl rsa -in /etc/letsencrypt/live/$certificate_name/privkey.pem -out le-rsaprivkey.pem
# download DST Root CA X3 certificate from internet
curl https://ssl-tools.net/certificates/dac9024f54d8f6df94935fb1732638ca6ad77c13.pem > le-root-ca.pem
# combine all the certificates into final le-flexibee.pem
cat /etc/letsencrypt/live/$certificate_name/fullchain.pem le-root-ca.pem le-rsaprivkey.pem > le-flexibee.pem

#now we have Let's Encrypt certificate suitable for FlexiBee, we can upload this certificate to server
curl -X PUT -u $flexibee_name:$flexibee_pass -k -L -T le-flexibee.pem https://localhost:5434/certificate
