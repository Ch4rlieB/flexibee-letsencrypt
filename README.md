# flexibee-letsencrypt
Script for automatic creating and uploading Let's Encrypt SSL certificate to czech accounting software ABRA FlexiBee.  
More info in article [Let's Encrypt SSL certificate in ABRA FlexiBee](https://charlieblog.eu/clanek-flexibee-a-lets-encrypt-ssl-certifikat).  
Big thanks to [Igi Hák](https://www.viry.cz/o-portalu-viry-cz/).

## Cofiguration
Set the correct values of *certificate_name*, *flexibee_name* and *flexibee_pass* variables.  
The URL address of FlexiBee server can be also changed if is not *localhost*.

## Usage
```shell
sudo ./flexibee-letsencrypt.sh
```
This script must be runned as root. Access to Let's Encrypt certificate directory is restricted.  
Script in current directory creates some certificates. The one named **le-flexibee.pem** is the correct and suitable for ABRA FlexiBee.

## Automatization
Add this script execution to cron.
```shell
sudo crontab -e
```
Add to this file new line
```shell
0 0 * * 0 /path/to/your/flexibee-letsencrypt.sh
```
This executes flexibee-letsencrypt.sh script at 00:00 on every Sunday.